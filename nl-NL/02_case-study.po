# Patrick Kox <patrick.kox@proximus.be>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2020-05-17 17:54+0200\n"
"PO-Revision-Date: 2017-04-29 16:03+0000\n"
"Last-Translator: Patrick Kox <patrick.kox@skynet.be>\n"
"Language-Team: Dutch <https://hosted.weblate.org/projects/debian-handbook/02_case-study/nl/>\n"
"Language: nl-NL\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.14-dev\n"

msgid "Falcot Corp"
msgstr "Falcon Corp"

msgid "SMB"
msgstr "SMB"

msgid "Strong Growth"
msgstr "Sterke Groei"

msgid "Master Plan"
msgstr "Master Plan"

msgid "Migration"
msgstr "Migratie"

msgid "Cost Reduction"
msgstr "Kosten Besparing"

msgid "Presenting the Case Study"
msgstr "Het Casusonderzoek Presenteren"

#, fuzzy
#| msgid "In the context of this book, you are the system administrator of a growing small business. The time has come for you to redefine the information systems master plan for the coming year in collaboration with your directors. You choose to progressively migrate to Debian, both for practical and economical reasons. Let's see in more detail what's in store for you…"
msgid "In the context of this book, you are the system administrator of a growing small business. The time has come for you to redefine the information systems master plan for the coming year in collaboration with your directors. You choose to progressively migrate to Debian, both for practical and economical reasons. Let's see in more detail what is in store for you…"
msgstr "In de context van dit boek ben jij de systeembeheerder van een groeiende KMO. Het is tijd om samen met de directie het masterplan voor de informatie systemen voor het komende jaar te her-definieren. Je kiest ervoor om progressief naar Debian te migreren, om zowel praktische als economische redenen. Laten we in meer detail kijken naar wat er je te wachten staat…"

msgid "We have envisioned this case study to approach all modern information system services currently used in a medium sized company. After reading this book, you will have all of the elements necessary to install Debian on your servers and fly on your own wings. You will also learn how to efficiently find information in the event of difficulties."
msgstr "We hebben dit casus-onderzoek zo gemaakt dat het allemaal moderne informatie systeem diensten die momenteel in KMO's gebruikt worden benaderd. Na het lezen van dit boek zou je in staat moeten zijn ontwikkelaars Debian te installeren op jouw servers en zelfstandig te werken. Je zal ook leren om efficiënt informatie te vinden bij problemen."

msgid "Fast Growing IT Needs"
msgstr "Snel Groeiende IT Noodzakelijkheden"

msgid "Falcot Corp is a manufacturer of high quality audio equipment. The company is growing strongly, and has two facilities, one in Saint-Étienne, and another in Montpellier. The former has around 150 employees; it hosts a factory for the manufacturing of speakers, a design lab, and all administrative office. The Montpellier site is smaller, with only about 50 workers, and produces amplifiers."
msgstr "Falcot Corp is een fabrikant van hoogwaardige audio apparatuur. Het bedrijf groeit snel en heeft twee bedrijven, één in Saint-Êtienne en een ander in Montpellier. De eerste heeft ongeveer 150 werknemers; het herbergt een fabriek waar luidsprekers gemaakt worden, een ontwerp lab en alle administratieve kantoren. De site in Montpellier is kleiner, met enkel 50 werknemers en maakt versterkers."

msgid "<emphasis>NOTE</emphasis> Fictional company created for case study"
msgstr "<emphasis>NOTITIE</emphasis> Fictief bedrijf gecreëerd voor casus-onderzoek"

msgid "The Falcot Corp company used as an example here is completely fictional. Any resemblance to an existing company is purely coincidental. Likewise, some example data throughout this book may be fictional."
msgstr "Het Falcot Corp bedrijf hier gebruikt als voorbeeld is volledig fictief. Iedere overeenkomst met een bestaand bedrijf is puur toevallig. Ook is bepaalde voorbeeld data gebruikt doorheen dit boek fictief."

#, fuzzy
#| msgid "The computer system has had difficulty keeping up with the company's growth, so they are now determined to completely redefine it to meet various goals established by management:"
msgid "The information system has had difficulty keeping up with the company's growth, so they are now determined to completely redefine it to meet various goals established by management:"
msgstr "Het computer systeem heeft het moeilijk om de groei van het bedrijf te volgen, dus ze zijn nu vastbesloten om het volledig te her-definieren om aan de verschillende doelen vastgelegd door de directie to voldoen:"

msgid "modern, easily scalable infrastructure;"
msgstr "modern, gemakkelijk te schalen infrastructuur;"

msgid "reducing cost of software licenses thanks to use of Open Source software;"
msgstr "De kost van software licenties reduceren dankzij het gebruik van Open Bron software;"

msgid "installation of an e-commerce website, possibly B2B (business to business, i.e. linking of information systems between different companies, such as a supplier and its clients);"
msgstr "installatie van een e-commerce website, waarschijnlijk B2B (business to business, d.w.z. informatie systemen tussen verschillende bedrijven koppelen, zoals een aanbieder en zijn klanten);"

msgid "significant improvement in security to better protect trade secrets related to new products."
msgstr "significante verbetering in beveiliging om bedrijfs-geheimen in verband met nieuwe producten beter te beschermen."

msgid "The entire information system will be overhauled with these goals in mind."
msgstr "Het volledig informatie systeem zal vernieuwd worden met deze doelen in gedachte."

msgid "<primary>master plan</primary>"
msgstr "<primary>master plan</primary>"

msgid "<primary>migration</primary>"
msgstr "<primary>migratie</primary>"

msgid "With your collaboration, IT management has conducted a slightly more extensive study, identifying some constraints and defining a plan for migration to the chosen Open Source system, Debian."
msgstr "Met jouw samenwerking heeft het IT management een iets uitgebreidere studie uitgevoerd, bepaalde beperkingen geconstateerd en een plan gedefinieerd voor migratie naar het gekozen Open Bron systeem, Debian."

msgid "A significant constraint identified is that the accounting department uses specific software, which only runs on <trademark>Microsoft Windows</trademark>. The laboratory, for its part, uses computer aided design software that runs on <trademark>OS X</trademark>."
msgstr "Een significante beperking die is geconstateerd is dat de boekhouding specifieke software gebruikt, welke enkel werkt op <trademark>Microsoft Windows</trademark>. Het labo, voor zijn part, gebruikt computer aided design software (CAD) die werkt op <trademark>OS X</trademark>."

msgid "Overview of the Falcot Corp network"
msgstr "Overzicht van het Falcot Corp netwerk"

msgid "The switch to Debian will be gradual; a small business, with limited means, cannot reasonably change everything overnight. For starters, the IT staff must be trained in Debian administration. The servers will then be converted, starting with the network infrastructure (routers, firewalls, etc.) followed by the user services (file sharing, Web, SMTP, etc.). Then the office computers will be gradually migrated to Debian, for each department to be trained (internally) during the deployment of the new system."
msgstr "De omschakeling naar Debian zal geleidelijk gebeuren; een klein bedrijf, met beperkte middelen, kan niet alles ineens veranderen. Ten eerste, het IT personeel moet getraind worden in Debian beheer. De server zullen geconverteerd moeten worden, te beginnen met de netwerk infrastructuur (routers, firewalls, enz.) gevolgd door de gebruikers diensten (bestanden delen, web, SMTP, enz.). Dan zullen de kantoor computers geleidelijk migreren naar Debian, want iedere afdeling moet (intern) opgeleid worden tijdens de implementatie van het nieuwe systeem."

msgid "Why a GNU/Linux Distribution?"
msgstr "Waarom een GNU/Linux Distributie?"

msgid "<emphasis>BACK TO BASICS</emphasis> Linux or GNU/Linux?"
msgstr "<emphasis>TERUG NAAR DE BASIS</emphasis> Linux of GNU/Linux?"

msgid "<primary>GNU/Linux</primary>"
msgstr "<primary>GNU/Linux</primary>"

msgid "<primary>Linux</primary>"
msgstr "<primary>Linux</primary>"

msgid "Linux, as you already know, is only a kernel. The expressions, “Linux distribution” and “Linux system” are, thus, incorrect: they are, in reality, distributions or systems <emphasis>based on</emphasis> Linux. These expressions fail to mention the software that always completes this kernel, among which are the programs developed by the GNU Project. Dr. Richard Stallman, founder of this project, insists that the expression “GNU/Linux” be systematically used, in order to better recognize the important contributions made by the GNU Project and the principles of freedom upon which they are founded."
msgstr "Linux, zoals je al weet, is enkel een kernel. De uitdrukkingen “Linux distributie” en “Linux systeem” zijn dus niet correct: ze zij, in werkelijkheid, distributies of systemen <emphasis>gebaseerd op</emphasis> Linux. Deze uitdrukkingen laten na om de software die de kernel altijd vervolledigd te noemen, waaronder zich de programma's ontwikkeld door het GNU Project bevinden. Dr. Richard Stallman, oprichter van dit project, staat er op dat de uitdrukking “GNU/Linux” systematisch wordt gebruikt, om de belangrijke bijdragen gemaakt door het GNU Project te erkennen en de principes van vrijheid waar ze of ge-fundeert zijn."

#, fuzzy
#| msgid "Debian has chosen to follow this recommendation, and, thus, name its distributions accordingly (thus, the latest stable release is Debian GNU/Linux 8)."
msgid "Debian has chosen to follow this recommendation, and, thus, name its distributions accordingly (thus, the latest stable release is Debian GNU/Linux 10)."
msgstr "Debian heeft ervoor gekozen om deze aanbeveling te volgen en daarom zijn distributie overeenkomstig te noemen (dus de laatste stabiele vrijgave is Debian GNU/Linux 8)."

msgid "Several factors have dictated this choice. The system administrator, who was familiar with this distribution, ensured it was listed among the candidates for the computer system overhaul. Difficult economic conditions and ferocious competition have limited the budget for this operation, despite its critical importance for the future of the company. This is why Open Source solutions were swiftly chosen: several recent studies indicate they are less expensive than proprietary solutions while providing equal or better quality of service so long as qualified personnel are available to run them."
msgstr "Verschillende factoren hebben deze keuze gedicteerd. De systeem beheerder, die bekend was met deze distributie, verzekerde dat het op de lijst van kandidaten voor de revisie van het computer systeem stond. Moeilijke economische omstandigheden en wrede concurrentie hebben het budget voor deze operatie gelimiteerd, ondanks de kritieke noodzaak voor de toekomst van het bedrijf. Dit is waarom Open Bron oplossingen snel werden gekozen: verschillende recente onderzoeken wijzen uit dat ze goedkoper zijn dan gepatenteerde oplossingen terwijl ze dezelfde of betere kwaliteit van diensten leveren zolang er gekwalificeerd personeel is om er mee te werken."

msgid "<emphasis>IN PRACTICE</emphasis> Total cost of ownership (TCO)"
msgstr "<emphasis>IN DE PRAKTIJK</emphasis> Total cost of ownership (TCO)"

msgid "<primary>TCO</primary>"
msgstr "<primary>TCO</primary>"

msgid "<primary>Total Cost of Ownership</primary>"
msgstr "<primary>Total Cost of Ownership</primary>"

msgid "The Total Cost of Ownership is the total of all money expended for the possession or acquisition of an item, in this case referring to the operating system. This price includes any possible license fee, costs for training personnel to work with the new software, replacement of machines that are too slow, additional repairs, etc. Everything arising directly from the initial choice is taken into account."
msgstr "De Total Cost of Ownership is het totaal van al het geld uitgegeven voor het bezit or aankoop van een item, in dit geval verwijst het naar het besturingssysteem. Deze prijs is inclusief iedere mogelijke licentie kost, kost voor opleiding van personeel om met de nieuwe software werken, vervangen van machines die te traag zijn, bijkomende raparaties, enz. Alles wat rechtstreeks voortkomt uit de initiële keuze wordt in rekening genomen."

msgid "This TCO, which varies according to the criteria chosen in the assessment thereof, is rarely significant when taken in isolation. However, it is very interesting to compare TCOs for different options if they are calculated according to the same rules. This assessment table is, thus, of paramount importance, and it is easy to manipulate it in order to draw a predefined conclusion. Thus, the TCO for a single machine doesn't make sense, since the cost of an administrator is also reflected in the total number of machines they manage, a number which obviously depends on the operating system and tools proposed."
msgstr "Deze TCO, welke varieert naar gelang de gekozen criteria in de beoordeling ervan, is zelden belangrijk wanneer het geïsoleerd wordt. Maar, is zeer interessant om de TCO's van verschillende opties te vergelijken, als ze berekend worden volgens dezelfde regels. Deze beoordelings-tabel is dus van het allergrootste belang en is gemakkelijk te manipuleren om aan een vooraf aangegeven conclusie te komen. Dus, de TCO voor één enkele machine is zinloos, omdat de kost van een beheerder altijd wordt weergegeven in het totaal van de machines die zij beheren, een aantal dat duidelijk afhangt van het besturingssysteem en de voorgestelde gereedschappen."

msgid "Among free operating systems, the IT department looked at the free BSD systems (OpenBSD, FreeBSD, and NetBSD), GNU Hurd, and Linux distributions. GNU Hurd, which has not yet released a stable version, was immediately rejected. The choice is simpler between BSD and Linux. The former have many merits, especially on servers. Pragmatism, however, led to choosing a Linux system, since its installed base and popularity are both very significant and have many positive consequences. One of these consequences is that it is easier to find qualified personnel to administer Linux machines than technicians experienced with BSD. Furthermore, Linux adapts to newer hardware faster than BSD (although they are often neck and neck in this race). Finally, Linux distributions are often more adapted to user-friendly graphical user interfaces, indispensable for beginners during migration of all office machines to a new system."
msgstr "Tussen de vrije besturingssystem heeft de IT afdeling gekeken naar de vrije BSD systemen (OpenBSD, FreeBSD en NetBSD), GNU Hurd en Linux distributies. GNU Hurd, wat nog geen stabiele versie heeft vrijgegeven, is direct verworpen, de keuze is gemakkelijker tussen BSD en Linux. De eerste heeft veel verdiensten, vooral op servers. Maar pragmatisme heeft geleid tot de keuze van een Linux systeem omdat zijn geïnstalleerde basis en populariteit beiden heel significant zijn en vele positieve gevolgen hebben. Een van deze gevolgen is dat het gemakkelijk is om gekwalificeerd personeel te vinden om Linux machines te beheren dan technici met ervaring met BSD. Voorts past Linux zich sneller aan aan nieuwe hardware dan BSD (hoewel ze vaak nek aan nek gaan in deze race). Tenslotte, Linux distributies zijn meer aangepast aan gebruiks-vriendelijke grafische gebruikers interfaces, onmisbaar voor beginners tijdens de migratie van alle kantoor machines naar een nieuw systeem."

msgid "<emphasis>ALTERNATIVE</emphasis> Debian GNU/kFreeBSD"
msgstr "<emphasis>ALTERNATIEF</emphasis> Debian GNU/kFreeBSD"

msgid "<primary>kFreeBSD</primary>"
msgstr "<primary>kFreeBSD</primary>"

msgid "<primary>FreeBSD</primary>"
msgstr "<primary>FreeBSD</primary>"

msgid "<primary>BSD</primary>"
msgstr "<primary>BSD</primary>"

#, fuzzy
#| msgid "<primary>migration</primary>"
msgid "<primary><literal>ports.debian.org</literal></primary>"
msgstr "<primary>migratie</primary>"

#, fuzzy
#| msgid "Since Debian <emphasis role=\"distribution\">Squeeze</emphasis>, it is possible to use Debian with a FreeBSD kernel on 32 and 64 bit computers; this is what the <literal>kfreebsd-i386</literal> and <literal>kfreebsd-amd64</literal> architectures mean. While these architectures are not “official release architectures”, about 90 % of the software packaged by Debian is available for them."
msgid "Since Debian 6 <emphasis role=\"distribution\">Squeeze</emphasis>, it is possible to use Debian with a FreeBSD kernel on 32 and 64 bit computers; this is what the <literal>kfreebsd-i386</literal> and <literal>kfreebsd-amd64</literal> architectures mean. While these architectures are not “official” (they are hosted on a separate mirror — <literal>ports.debian.org</literal>), they provide over 70% of the software packaged by Debian."
msgstr "Sinds Debian <emphasis role=\"distribution\">Squeeze</emphasis> is het mogelijk om Debian te gebruiken met een FreeBSD kernel op 32 en 64 bit computers; dit is wat de <literal>kfreebsd-i386</literal> en <literal>kfreebsd-amd64</literal> architecturen betekenen. Terwijl deze architecturen geen “officiële vrijgave architecturen” zijn, is ongeveer 90% van de software pakketten door Debian er op beschikbaar."

msgid "These architectures may be an appropriate choice for Falcot Corp administrators, especially for a firewall (the kernel supports three different firewalls: IPF, IPFW, PF) or for a NAS (network attached storage system, for which the ZFS filesystem has been tested and approved)."
msgstr "Deze architecturen mogen dan een geschikte keuze zijn voor Falcot Corp beheerders, vooral voor een firewall (de kernel ondersteund drie verschillende firewalls: IPF, IPFW en PF) of voor een NAS (Network Attached Storage systeem, waarvoor het ZFS bestandssysteem is getest en goedgekeurd)."

msgid "Why the Debian Distribution?"
msgstr "Waarom de Debian Distributie?"

msgid "Once the Linux family has been selected, a more specific option must be chosen. Again, there are plenty of criteria to consider. The chosen distribution must be able to operate for several years, since the migration from one to another would entail additional costs (although less than if the migration were between two totally different operating systems, such as Windows or OS X)."
msgstr "Eenmaal de Linux familie is geselecteerd moet een meer specifieke optie gekozen worden. Nogmaals, er zijn vele criteria te overwegen. De gekozen distributie moet voor meerdere jaren dienst doen, omdat de migratie van de ene naar de andere extra kosten met zich mee brengt (alhoewel minder dan indien de migratie tussen twee totaal verschillende besturingssystemen zou plaatsvinden, zoals Windows of OS X)."

msgid "Sustainability is, thus, essential, and it must guarantee regular updates and security patches over several years. The timing of updates is also significant, since, with so many machines to manage, Falcot Corp can not handle this complex operation too frequently. The IT department, therefore, insists on running the latest stable version of the distribution, benefiting from the best technical assistance, and guaranteed security patches. In effect, security updates are generally only guaranteed for a limited duration on older versions of a distribution."
msgstr "Duurzaamheid is dus essentieel en het moet de garantie bieden van regelmatige updates en beveiligings-patches gedurende meerdere jaren. De timing van updates is ook significant, omdat met zoveel machines te beheren, Falcor Corp deze complexe operatie niet al te frequent kan uitvoeren. De IT afdeling, staat er dus op om de laatste stabiele versie van de distributie te gebruiken, genietend van de beste technische ondersteuning, en met de garantie van beveiligings-patches. In feite worden beveiligings-updates enkel gegarandeerd voor een beperkte tijd op oudere versies van een distributie."

#, fuzzy
#| msgid "Finally, for reasons of homogeneity and ease of administration, the same distribution must run on all the servers (some of which are Sparc machines, currently running Solaris) and office computers."
msgid "Finally, for reasons of homogeneity and ease of administration, the same distribution must run on all the servers and office computers."
msgstr "Tenslotte, om homogeniteits-redenen en gemak van administratie, moet dezelfde distributie draaien op alle servers (sommigen daarvan zijn Sparc machines, die momenteel Solaris draaien) en op alle kantoor computers."

msgid "Commercial and Community Driven Distributions"
msgstr "Commerciële en Gemeenschaps-Gedreven Distributies"

msgid "There are two main categories of Linux distributions: commercial and community driven. The former, developed by companies, are sold with commercial support services. The latter are developed according to the same open development model as the free software of which they are comprised."
msgstr "Er zijn twee grote categorieën van Linux distributies: commercieel en gemeenschaps-gedreven. De eerste, ontworpen door bedrijven, worden verkocht met commerciële ondersteunings-diensten. De laatste worden ontwikkeld volgens het zelfde open ontwikkel model als de vrije software waaruit ze bestaan."

#, fuzzy
#| msgid "A commercial distribution will have, thus, a tendency to release new versions more frequently, in order to better market updates and associated services. Their future is directly connected to the commercial success of their company, and many have already disappeared (Caldera Linux, StormLinux, etc.)."
msgid "A commercial distribution will have, thus, a tendency to release new versions more frequently, in order to better market updates and associated services. Their future is directly connected to the commercial success of their company, and many have already disappeared (Caldera Linux, StormLinux, Mandriva Linux, etc.)."
msgstr "Een commerciële distributie zal dus de neiging hebben om frequenter nieuwe versies vrij te geven, om updates en bijbehorende diensten te promoten. Hun toekomst is rechtstreeks verbonden met het commerciële succes van hun bedrijf, en er zijn er al veel verdwenen (Caldera Linux, StormLinux, enz.)."

msgid "A community distribution doesn't follow any schedule but its own. Like the Linux kernel, new versions are released when they are stable, never before. Its survival is guaranteed, as long as it has enough individual developers or third party companies to support it."
msgstr "Een gemeenschaps-distributie volgt geen ander schema dan dat van zichzelf. Zoals de Linux kernel, nieuwe versies worden vrijgegeven wanneer ze stabiel zijn, nooit eerder. Haar overleving is gegarandeerd, zolang het voldoende individuele ontwikkelaars of externe bedrijven die haar steunen heeft."

msgid "<primary>distribution</primary><secondary>community Linux distribution</secondary>"
msgstr "<primary>distributie</primary><secondary>gemeenschaps-Linux distributie</secondary>"

msgid "<primary>distribution</primary><secondary>commercial Linux distribution</secondary>"
msgstr "<primary>distributie</primary><secondary>commerciële Linux distributie</secondary>"

msgid "A comparison of various Linux distributions led to the choice of Debian for various reasons:"
msgstr "Een vergelijking van verschillende Linux distributies heeft geleid tot de keuze voor Debian voor verschillende redenen:"

msgid "It is a community distribution, with development ensured independently from any commercial constraints; its objectives are, thus, essentially of a technical nature, which seem to favor the overall quality of the product."
msgstr "Het is een gemeenschaps-distributie, met verzekerde ontwikkeling onafhankelijk van iedere commerciële beperking; haar doelen zijn, dus in essentie van een technologische aard, wat de algemene kwaliteit van het product ten goede komt."

msgid "Of all community distributions, it is the most significant from many perspectives: in number of contributors, number of software packages available, and years of continuous existence. The size of its community is an incontestable witness to its continuity."
msgstr "Van alle gemeenschaps-distributies, is het de meest significante van meerdere perspectieven: in aantal bijdragers, aantal beschikbare software pakketten en jaren van doorlopend bestaan. De grote van haar gemeenschap is een onbetwistbare getuige van haar continuïteit."

msgid "Statistically, new versions are released every 18 to 24 months, and they are supported for 5 years, a schedule which is agreeable to administrators."
msgstr "Statistisch gezien wordt iedere 18 tot 24 maanden een nieuwe versie vrijgegeven, en ze worden 5 jaar ondersteund, een schema maar administrators akkoord kunnen gaan."

msgid "A survey of several French service companies specialized in free software has shown that all of them provide technical assistance for Debian; it is also, for many of them, their chosen distribution, internally. This diversity of potential providers is a major asset for Falcot Corp's independence."
msgstr "Een enquête bij verschillende Franse diensten bedrijven gespecialiseerd in vrije software heeft aangetoond dat ze allemaal technische ondersteuning bieden voor Debian; het is ook, voor vele onder hen, hun gekozen interne distributie. Deze diversiteit aan mogelijke aanbieders is een grote aanwinst voor de onafhankelijkheid van Falcot Corp."

msgid "Finally, Debian is available on a multitude of architectures, including ppc64el for OpenPOWER processors; it will, thus, be possible to install it on Falcot Corp's latest IBM servers."
msgstr "Tenslotte, Debian is beschikbaar op meerdere architecturen, inclusief ppc64el voor OpenPOWER processoren; het zal dus mogelijk zijn om het eventueel te installeren op de nieuwste IBM servers van Falcot Corp."

msgid "<emphasis>IN PRACTICE</emphasis> Debian Long Term Support"
msgstr "<emphasis>IN DE PRAKTIJK</emphasis> Debian Lange Termijn Ondersteuning"

msgid "The Debian Long Term Support (LTS) project started in 2014 and aims to provide 5 years of security support to all stable Debian releases. As LTS is of primary importance to organizations with large deployments, the project tries to pool resources from Debian-using companies. <ulink type=\"block\" url=\"https://wiki.debian.org/LTS\" />"
msgstr "Het Debian Lange Termijn Ondersteunings (LTS) project is gestart in 2014 en probeert 5 jaar lang beveiligings-ondersteuning te bieden aan alle stabiele Debian vrijgaves. Sinds LTS vooral hoofdzakelijk belangrijk is voor bedrijven met grote implementaties, probeert het project middelen te verkrijgen van bedrijven die Debian gebruiken. <ulink type=\"block\" url=\"https://wiki.debian.org/LTS\" />"

#, fuzzy
#| msgid "Falcot Corp is not big enough to let one member of its IT staff contribute to the LTS project, so the company opted to subscribe to Freexian's Debian LTS contract and provides financial support. Thanks to this, the Falcot administrators know that the packages they use will be handled in priority and they have a direct contact with the LTS team in case of problems. <ulink type=\"block\" url=\"https://wiki.debian.org/LTS/Funding\" /> <ulink type=\"block\" url=\"http://www.freexian.com/services/debian-lts.html\" />"
msgid "Falcot Corp is not big enough to let one member of its IT staff contribute to the LTS project, so the company opted to subscribe to Freexian's Debian LTS contract and provides financial support. Thanks to this, the Falcot administrators know that the packages they use will be handled in priority and they have a direct contact with the LTS team in case of problems. <ulink type=\"block\" url=\"https://wiki.debian.org/LTS/Funding\" /> <ulink type=\"block\" url=\"https://www.freexian.com/services/debian-lts.html\" />"
msgstr "Falcit Corp is niet groot genoeg om één personeelslid van haar IT afdeling te laten bijdragen aan het LTS project, dus heeft het bedrijf ervoor gekozen om een abonnement te nemen op een Debian LTS contract van Freexian en bied financiële steun. Hierdoor weten de Falcot beheerders dat de pakketten die zij gebruiken met prioriteit behandeld worden en ze hebben direct contact met het LTS team in geval van een probleem. <ulink type=\"block\" url=\"https://wiki.debian.org/LTS/Funding\" /> <ulink type=\"block\" url=\"http://www.freexian.com/services/debian-lts.html\" />"

#, fuzzy
#| msgid "Once Debian has been chosen, the matter of which version to use must be decided. Let us see why the administrators have picked Debian Jessie."
msgid "Once Debian has been chosen, the matter of which version to use must be decided. Let us see why the administrators have picked Debian <emphasis role=\"distribution\">Buster</emphasis>."
msgstr "Eenmaal Debian is gekozen, de kwestie van welke versie te gebruiken dient zich aan. Laten we kijken waarom de beheerders hebben gekozen voor Debian Jessie."

#, fuzzy
#| msgid "Why Debian Jessie?"
msgid "Why Debian Buster?"
msgstr "Waarom Debian Jessie?"

#, fuzzy
#| msgid "Every Debian release starts its life as a continuously changing distribution, also known as “<emphasis role=\"distribution\">Testing</emphasis>”. But at the time we write those lines, Debian Jessie is the latest “<emphasis role=\"distribution\">Stable</emphasis>” version of Debian."
msgid "Every Debian release starts its life as a continuously changing distribution, also known as “<emphasis role=\"distribution\">Testing</emphasis>”. But at the time you read those lines, Debian <emphasis role=\"distribution\">Buster</emphasis> is the latest “<emphasis role=\"distribution\">Stable</emphasis>” version of Debian."
msgstr "Iedere Debian vrijgave start haar leven als een continu veranderende distributie, ook gekent als “<emphasis role=\"distribution\">Testen</emphasis>”. Maar op het moment dat ze deze regels scrijven is Debian Jessie de laatste <emphasis role=\"distribution\">“Stabiele”</emphasis> versie van Debian."

#, fuzzy
#| msgid "The choice of Debian Jessie is well justified based on the fact that any administrator concerned about the quality of their servers will naturally gravitate towards the stable version of Debian. Even if the previous stable release might still be supported for a while, Falcot administrators aren't considering it because its support period will not last long enough and because the latest version brings new interesting features that they care about."
msgid "The choice of Debian <emphasis role=\"distribution\">Buster</emphasis> is well justified based on the fact that any administrator concerned about the quality of their servers will naturally gravitate towards the stable version of Debian. Even if the previous stable release might still be supported for a while, Falcot administrators aren't considering it because its support period will not last long enough and because the latest version brings new interesting features that they care about."
msgstr "De keuze voor Debian Jessie is gerechtigd gebaseerd op het feit dat iedere beheerder bezorgd om de kwaliteit van hun servers automatisch naar een stabiele versie van Debian aangetrokken worden. Zelfs indien de vorige stabiele vrijgave nog een hele tijd ondersteund zal worden, de beheerders van Falcot houden er geen rekening mee omdat de ondersteunings-periode niet lang genoeg zal duren en omdat de laatste versie nieuwe interessante kenmerken, waar ze om geven, heeft."
